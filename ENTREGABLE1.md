# ALUMNO BRAYAN OSCAR QUISPE ROCA



# Ejecutar operaciones básicas tipo DCL y TCL para crear procedimientos y funciones. 
 **1. Crea un procedimiento para ingresar registros usando comandos DCL.**
 
 **2. Crear un procedimiento almacenado con control de errores usando comandos TCL.**
***Crear un procedimiento para ingresar registros en la tabla articulo***
~~~ 
CREATE PROCEDURE InsertarArticulo(
    @idcategoria INTEGER,
    @codigo TEXT,
    @nombre TEXT,
    @precio_venta REAL,
    @stock INTEGER,
    @descripcion TEXT,
    @imagen TEXT
)
AS
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen)
    VALUES (@idcategoria, @codigo, @nombre, @precio_venta, @stock, @descripcion, @imagen);
END;
~~~
 ***Crear un procedimiento almacenado con control de errores para realizar una venta***
~~~
CREATE PROCEDURE RealizarVenta(
    @idcliente INTEGER,
    @idusuario INTEGER,
    @tipo_comprobante TEXT,
    @serie_comprobante TEXT,
    @num_comprobante TEXT,
    @detalle_venta TABLE( idarticulo INTEGER, cantidad INTEGER, precio REAL, descuento REAL )
)
AS
BEGIN
    -- Iniciar una transacción
    BEGIN TRANSACTION;
~~~
***Insertar la venta en la tabla venta***
  ~~~  
    INSERT INTO venta (idcliente, idusuario, tipo_comprobante, serie_comprobante, num_comprobante, fecha, impuesto, total, estado)
    VALUES (@idcliente, @idusuario, @tipo_comprobante, @serie_comprobante, @num_comprobante, DATETIME('now'), 0.00, 0.00, 'Pendiente');

    -- Obtener el último ID de venta insertado
    DECLARE @idventa INTEGER;
    SELECT @idventa = last_insert_rowid();
~~~
***Insertar los detalles de la venta en la tabla detalle_venta***
~~~
    
    INSERT INTO detalle_venta (idventa, idarticulo, cantidad, precio, descuento)
    SELECT @idventa, idarticulo, cantidad, precio, descuento FROM @detalle_venta;
~~~
***Actualizar el total y el impuesto de la venta***
~~~
    
    UPDATE venta
    SET total = (SELECT SUM(cantidad * precio - descuento) FROM detalle_venta WHERE idventa = @idventa),
        impuesto = (SELECT total * 0.18 FROM venta WHERE idventa = @idventa)
    WHERE idventa = @idventa;

    -- Finalizar la transacción
    COMMIT;
END;

~~~


# Crear restricciones al modelo de BD, para asegurar la calidad de la información. 
**1. Usar tipo de datos para optimizar consultas y el uso de memoria RAM.** 

**2. Usar de restricciones tipo DEFAULT, CHECK, UNIQUE, IDENTITY.** 
***Ejemplo de restricción de clave primaria en la tabla categoria***
~~~
CREATE TABLE categoria (
    idcategoria INTEGER PRIMARY KEY AUTOINCREMENT,
    ...
);
~~~
***Ejemplo de restricción de clave foránea en la tabla articulo***
~~~

CREATE TABLE articulo (
    ...
    idcategoria INTEGER NOT NULL,
    FOREIGN KEY (idcategoria) REFERENCES categoria(idcategoria)
);
~~~
***Ejemplo de uso de tipo de datos INTEGER para el campo stock en la tabla articulo***
~~~

CREATE TABLE articulo (
    ...
    stock INTEGER CHECK (stock >= 0),
    ...
);
~~~
***Ejemplo de restricciones DEFAULT, CHECK y UNIQUE en la tabla articulo***
~~~

CREATE TABLE articulo (
    ...
    precio_venta REAL DEFAULT 0.0 CHECK (precio_venta >= 0),
    codigo TEXT UNIQUE,
    ...
);
~~~
***Ejemplo de uso de AUTOINCREMENT en la tabla categoria***
~~~
CREATE TABLE categoria (
    idcategoria INTEGER PRIMARY KEY AUTOINCREMENT,
    ...
);
~~~

# Desarrolla programas de Transact SQL para la creación de una tienda virtual. 
**1. Crear store procedures con variables locales, parámetros de entrada y de salida** 

**2. Crear funciones con variables locales, parámetros de entrada y de salida** 

**3. Aplicar sentencias condicionales y sentencias repetitiva en un programa Transact SQL** 

~~~
CREATE PROCEDURE BuscarArticuloPorCodigo 
    @CodigoArticulo TEXT,
    @NombreArticulo TEXT OUT,
    @PrecioArticulo REAL OUT
AS
BEGIN
    DECLARE @StockArticulo INTEGER;
~~~
***Buscar el artículo por su código***
~~~
    SELECT @NombreArticulo = nombre, @PrecioArticulo = precio_venta, @StockArticulo = stock
    FROM articulo
    WHERE codigo = @CodigoArticulo;
~~~
***Verificar si se encontró el artículo***
~~~
    IF @@ROWCOUNT = 0
    BEGIN
        PRINT 'El artículo con código ' + @CodigoArticulo + ' no fue encontrado.';
    END
    ELSE
    BEGIN
        PRINT 'Nombre del artículo: ' + @NombreArticulo;
        PRINT 'Precio del artículo: ' + CAST(@PrecioArticulo AS VARCHAR);
        PRINT 'Stock disponible: ' + CAST(@StockArticulo AS VARCHAR);
    END
END;
~~~
~~~
CREATE FUNCTION CalcularTotalVenta 
    (@IdVenta INTEGER)
RETURNS REAL
AS
BEGIN
    DECLARE @TotalVenta REAL;
~~~
***Calcular el total de la venta***
~~~
    SELECT @TotalVenta = SUM((cantidad * precio) - descuento)
    FROM detalle_venta
    WHERE idventa = @IdVenta;

    -- Retornar el total de la venta
    RETURN @TotalVenta;
END;
~~~
***Ejecutar el procedimiento almacenado***
~~~

DECLARE @NombreArticuloEncontrado TEXT;
DECLARE @PrecioArticuloEncontrado REAL;

EXEC BuscarArticuloPorCodigo 'PC001', @NombreArticuloEncontrado OUT, @PrecioArticuloEncontrado OUT;
~~~
***Imprimir los resultados***
~~~

PRINT 'Nombre del artículo: ' + @NombreArticuloEncontrado;
PRINT 'Precio del artículo: ' + CAST(@PrecioArticuloEncontrado AS VARCHAR);
~~~
***Ejecutar la función***
~~~
DECLARE @TotalVenta REAL;

SET @TotalVenta = dbo.CalcularTotalVenta(1);
~~~
***Imprimir el total de la venta***
~~~
PRINT 'Total de la venta: ' + CAST(@TotalVenta AS VARCHAR);
~~~

# Realiza consultas avanzadas en BD para la generación de reportes estadísticos. 

**- Recuperar e intersectar datos.** 

**- Manipular datos con consultas múltiples.** 

**- Usar sentencias GROUP BY y HAVING.**

**- Usar subconsultas con procedimientos almacenados.**

**- Creación y clasificación de vistas.**
***Obtener la cantidad de ventas realizadas por cada cliente***
~~~
SELECT p.nombre AS Cliente, COUNT(*) AS TotalVentas
FROM persona p
INNER JOIN venta v ON p.idpersona = v.idcliente
GROUP BY p.nombre;
~~~
***Obtener el total de ingresos y ventas realizadas en la tienda***
~~~
SELECT 'Ingresos' AS Tipo, SUM(total) AS Total
FROM ingreso
UNION ALL
SELECT 'Ventas' AS Tipo, SUM(total) AS Total
FROM venta;
~~~
***Obtener el total de ventas por tipo de comprobante, solo incluir aquellas con un total mayor a $500***
~~~
SELECT tipo_comprobante, SUM(total) AS TotalVentas
FROM venta
GROUP BY tipo_comprobante
HAVING SUM(total) > 500;
~~~
***Obtener el total de ventas realizadas por clientes con más de 5 compras***
~~~
SELECT p.nombre AS Cliente, (
    SELECT COUNT(*)
    FROM venta v
    WHERE v.idcliente = p.idpersona
) AS TotalVentas
FROM persona p
WHERE p.tipo_persona = 'Cliente'
AND (
    SELECT COUNT(*)
    FROM venta v
    WHERE v.idcliente = p.idpersona
) > 5;
~~~
***Crear vista para mostrar el stock actual de cada artículo***
~~~

CREATE VIEW VistaStockArticulos AS
SELECT idarticulo, nombre, stock
FROM articulo;
~~~

***Consultar la vista y ordenar los resultados por nombre del artículo***
~~~
SELECT * FROM VistaStockArticulos ORDER BY nombre;
~~~

